﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MvcMovie
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("second line");
            Console.WriteLine("This is done in testing branch");
            CreateWebHostBuilder(args).Build().Run();
            Console.WriteLine("This is hotfix branch");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
